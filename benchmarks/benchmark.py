# -*- coding: utf-8 -*-
"""Queue and concatenation benchmarks

Simple and imprecise benchmark of various queue push method, reversed queque
push methods, and string concatenation techniques.

Not very well done or informative."""
from collections import deque
from typing import Collection
from typing import Iterable
from typing import Iterator

from profilehooks import timecall

# you can also try profile instead of timecall for more info


@timecall
def funct_1_rightextend(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        final_deq.extend(iterable)
    return final_deq


@timecall
def funct_2_rightextend_reverse(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        final_deq.extend(reversed(iterable))
    return final_deq


@timecall
def funct_3_rightextend_listreverse(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        list_ = list(iterable)
        list_.reverse()
        final_deq.extend(list_)
    return final_deq


@timecall
def funct_4_rightextend_listslice(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        list_ = list(iterable)
        final_deq.extend(list_[::-1])
    return final_deq


@timecall
def funct_5_extendleft(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        final_deq.extendleft(iterable)
    return final_deq


@timecall
def funct_6_extendleft_reverse(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        final_deq.extendleft(reversed(iterable))
    return final_deq


@timecall
def funct_7_extendleft_listreverse(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        list_ = list(iterable)
        list_.reverse()
        final_deq.extendleft(list_)
    return final_deq


@timecall
def funct_8_extendleft_listslice(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        list_ = list(iterable)
        final_deq.extendleft(list_[::3])
    return final_deq


@timecall
def funct_9_extleft_concat(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        curr_deq = deque()  # type: deque
        curr_deq.extendleft(iterable)
        final_deq = final_deq + curr_deq
    return final_deq


@timecall
def funct_10_extleft_extright(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        curr_deq = deque()  # type: deque
        curr_deq.extendleft(iterable)
        final_deq.extend(curr_deq)
    return final_deq


@timecall
def funct_11_concat(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        curr_deq = deque(iterable)
        final_deq = curr_deq + final_deq
    return final_deq


@timecall
def funct_12_extleft_extleft(iterables: Collection[Iterable]) -> deque:
    final_deq = deque()  # type: deque
    for iterable in iterables:
        curr_deq = deque()  # type: deque
        curr_deq.extendleft(iterable)
        final_deq.extendleft(curr_deq)
    return final_deq


FUNCTIONS = [
    funct_1_rightextend,
    funct_2_rightextend_reverse,
    funct_3_rightextend_listreverse,
    funct_4_rightextend_listslice,
    funct_5_extendleft,
    funct_6_extendleft_reverse,
    funct_7_extendleft_listreverse,
    funct_8_extendleft_listslice,
    funct_9_extleft_concat,
    funct_10_extleft_extright,
    funct_11_concat,
    funct_12_extleft_extleft,
]


def make_range(n: int = 100_000) -> Iterator:
    return (range(x * 100_000) for x in range(1, 20))


def main():
    n = 100_000
    for func in FUNCTIONS:
        curr_res = func(make_range(n))
        if curr_res:
            print("ok")


if __name__ == "__main__":
    main()

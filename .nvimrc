" vim:set et sw=4 ts=4 tw=78:
set encoding=utf-8
scriptencoding utf-8


" Don't cd into the directory of the current file
let g:autocd = 0


let g:ale_linters.python = []
let g:ale_fixers.python = ['black', 'remove_trailing_lines']

let g:ncm2_jedi#environment = '.venv'

let g:ale_python_mypy_options = '--ignore-missing-imports --warn-redundant-casts'

let g:gutentags_file_list_command = {
    \ 'markers': {
        \ '.git': 'git ls-files',
        \ '.hg': 'hg files',
        \ },
\}

.. csnake documentation master file, created by
   sphinx-quickstart on Tue Aug 14 12:31:15 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. role:: ccode(code)
    :language: c

.. role:: pycode(code)
    :language: python

.. include:: ../README.rst

.. toctree::
   :caption: Contents:
   :hidden:

   api
   contributing
   changes
